package com.hh.emailservice.consumer;


import com.hh.emailservice.dto.ItemEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

@Service
public class ItemConsumer {

    private static final Logger LOGGER = LoggerFactory.getLogger(ItemConsumer.class);

    @RabbitListener(queues = {"${rabbitmq.queue.email.name}"})
    public void consume(ItemEvent itemEvent) {
        LOGGER.info(String.format("ItemEvent received in email service -> %s", itemEvent.toString()));

        // TODO send out email
    }

}

