package com.hh.producerservice.publisher;

import com.hh.producerservice.dto.ItemEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class ItemProducer {

    private static final Logger LOGGER = LoggerFactory.getLogger(ItemProducer.class);

    @Value("${rabbitmq.exchange.name}")
    private String exchange;

    @Value("${rabbitmq.binding.warehouse.routing.key}")
    private String warehouseRoutingKey;

    @Value("${rabbitmq.binding.email.routing.key}")
    private String emailRoutingKey;


    private RabbitTemplate rabbitTemplate;

    public ItemProducer(RabbitTemplate rabbitTemplate) {
        this.rabbitTemplate = rabbitTemplate;
    }

    public void sendMessage(ItemEvent itemEvent) {
        LOGGER.info(String.format("ItemEvent sent to RabbitMQ -> %s", itemEvent.toString()));

        // send an ItemEvent to warehouse queue
        rabbitTemplate.convertAndSend(exchange, warehouseRoutingKey, itemEvent);

        // send an ItemEvent to email queue
        rabbitTemplate.convertAndSend(exchange, emailRoutingKey, itemEvent);

    }

}

