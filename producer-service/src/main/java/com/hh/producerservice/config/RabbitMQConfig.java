package com.hh.producerservice.config;

import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class RabbitMQConfig {

    /**
     *   1. spring bean for queue - order queue
     *   2. spring bean for binding between exchange and queue using routing key
     *   3. message converter
     *   4. configure RabbitTemplate
     */

    @Value("${rabbitmq.exchange.name}")
    private String exchange;

    @Value("${rabbitmq.queue.warehouse.name}")
    private String warehouseQueue;

    @Value("${rabbitmq.binding.warehouse.routing.key}")
    private String warehouseRoutingKey;

    @Value("${rabbitmq.queue.email.name}")
    private String emailQueue;

    @Value("${rabbitmq.binding.email.routing.key}")
    private String emailRoutingKey;




    // spring bean for exchange
    @Bean
    public TopicExchange exchange() {
        return new TopicExchange(exchange);
    }



    // warehouse: spring bean for queue
    @Bean
    public Queue warehouseQueue() {
        return new Queue(warehouseQueue);
    }

    // warehouse: spring bean for binding between exchange and queue using routing key
    @Bean
    public Binding warehouseBinding() {
        return BindingBuilder
                .bind(warehouseQueue())
                .to(exchange())
                .with(warehouseRoutingKey);
    }



    // email: spring bean for queue
    @Bean
    public Queue emailQueue() {
        return new Queue(emailQueue);
    }

    // Email: spring bean for binding between exchange and queue using routing key
    @Bean
    public Binding emailBinding() {
        return BindingBuilder
                .bind(emailQueue())
                .to(exchange())
                .with(emailRoutingKey);
    }



    // message converter
    @Bean
    public MessageConverter converter() {
        return new Jackson2JsonMessageConverter();
    }

    // configure RabbitTemplate
    @Bean
    public AmqpTemplate amqpTemplate(ConnectionFactory connectionFactory) {
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(converter());
        return rabbitTemplate;
    }


}
