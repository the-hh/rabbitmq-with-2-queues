package com.hh.producerservice.controller;


import com.hh.producerservice.dto.Item;
import com.hh.producerservice.dto.ItemEvent;
import com.hh.producerservice.publisher.ItemProducer;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@RestController
@RequestMapping("/api/v1")
public class ItemController {

    private ItemProducer itemProducer;

    public ItemController(ItemProducer itemProducer) {
        this.itemProducer = itemProducer;
    }

    @PostMapping("/item")
    public String placeOrder(@RequestBody Item item) {
        item.setItemId(UUID.randomUUID().toString());

        ItemEvent itemEvent = new ItemEvent();
        itemEvent.setStatus("DISPATCHED");
        itemEvent.setMessage("Item is dispatched");
        itemEvent.setOrder(item);
        itemProducer.sendMessage(itemEvent);

        return "ItemEvent sent to the RabbitMQ...";
    }
}

