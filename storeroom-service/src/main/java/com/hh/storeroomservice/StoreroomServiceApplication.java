package com.hh.storeroomservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StoreroomServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(StoreroomServiceApplication.class, args);
	}

}
