package com.hh.storeroomservice.consumer;

import com.hh.storeroomservice.dto.ItemEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

@Service
public class ItemConsumer {

    private static final Logger LOGGER = LoggerFactory.getLogger(ItemConsumer.class);

    @RabbitListener(queues = {"${rabbitmq.queue.warehouse.name}"})
    private void consume(ItemEvent itemEvent) {
        LOGGER.info(String.format("ItemEvent received -> %s", itemEvent.toString()));

        // TODO: save item data to database
    }

}

